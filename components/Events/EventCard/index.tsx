import Image from 'next/image';
import { chakra, Box } from '@chakra-ui/react';
import { EventProp } from '../types';
import styles from './event.module.scss';

interface Props {
  event: EventProp;
}

export default function EventCard(props: Props) {
  const { event } = props;
  const {
    onClick,
    image,
    startDate,
    onlineOfflineText,
    source,
    title,
    type,
    url,
  } = event;

  return (
    <Box bg="white" className={styles.event} onClick={() => onClick(event)}>
      <a href={url} target="_blank" rel="noreferrer">
        <Image
          src={image}
          alt="Event image"
          width="100%"
          height="70px"
          layout="responsive"
        />
      </a>
      <Box px="15px" py="10px">
        <chakra.p py="5px">{onlineOfflineText}</chakra.p>
        <chakra.p py="5px">{startDate}</chakra.p>
        <chakra.p py="5px">
          <strong>{title}</strong>
        </chakra.p>
        <chakra.p p="0 0 10px 0">Source: {source}</chakra.p>
        <chakra.p>{type}</chakra.p>
      </Box>
    </Box>
  );
}
