import { chakra } from '@chakra-ui/react';
import Image from 'next/image';
import styles from './footer.module.scss';

interface Props {
  horizontalPadding: any;
}

export function SocialIcon() {
  return (
    <div className={styles.socialIcons}>
      <a href="https://twitter.com/VentureBitesCo" target="_blank">
        <Image
          src="/images/twitter.svg"
          alt="Twitter logo"
          width="40px"
          height="40px"
        />
      </a>

      <a href="https://t.me/joinchat/WLgqu9ISWkGVKRiu" target="_blank">
        <Image
          src="/images/telegram.svg"
          alt="Telegram logo"
          width="40px"
          height="40px"
        />
      </a>

      <a href="https://www.linkedin.com/company/venturebites/" target="_blank">
        <Image
          src="/images/linkedin.svg"
          alt="Linkedin logo"
          width="40px"
          height="40px"
        />
      </a>
    </div>
  );
}

function Footer(props: Props) {
  const { horizontalPadding } = props;

  return (
    <chakra.div
      px={horizontalPadding}
      py={{ base: '15px' }}
      className={styles.footer}
    >
      <div>Follow us on social media</div>

      <SocialIcon />
    </chakra.div>
  );
}

export default Footer;
