import { Box, chakra } from '@chakra-ui/react';
import * as dateFns from 'date-fns';
import { EventProp } from '../types';
import EventCard from '../EventCard';

interface Props {
  header: string | null;
  events: EventProp[];
  onClick: (event: EventProp) => void;
  eventType: 'upcoming' | 'ongoing';
}

export default function EventSearchResult(props: Props) {
  const { events, onClick, header, eventType } = props;
  return (
    <Box>
      <chakra.p m="0 0 10px 0">
        <strong>{header}</strong>
      </chakra.p>
      {/* <Box display="flex" flexWrap="wrap" justifyContent="space-between"> */}
      <Box
        display="grid"
        gridTemplateColumns={{
          base: '100%',
          md: 'repeat(2, 1fr)',
          lg: 'repeat(4, 1fr)',
        }}
        gridColumnGap={{ base: '10px', xl: '20px' }}
        gridRowGap="20px"
      >
        {events?.map((event: any) => {
          const {
            _id,
            image,
            title,
            isOnline,
            type,
            startDate,
            organizer,
            url,
            source,
          } = event;
          const parsedStartDate = dateFns.parseISO(startDate);
          const utcOffsetHours = (-1 * parsedStartDate.getTimezoneOffset()) / 60;
          const formattedStartDate = dateFns.format(
            parsedStartDate,
            'MMMM dd yyyy | p'
          );
          const formattedStartDateWithUtcOffset = `${formattedStartDate} UTC${utcOffsetHours > 0 ? `+${utcOffsetHours}` : utcOffsetHours}`

          const formattedEvent = {
            onClick,
            startDate: formattedStartDateWithUtcOffset,
            onlineOfflineText: isOnline ? 'Online' : 'Offline',
            title,
            organizer: organizer.text,
            type,
            image: image.large || image.medium || image.small,
            url,
            source,
          };

          return (
            <EventCard key={`${_id}-${eventType}`} event={formattedEvent} />
          );
        })}
      </Box>
    </Box>
  );
}
