export const config = {
  isLocalhost: process.env.NEXT_PUBLIC_API_URL?.includes('localhost'),
  mailchimpSignupFormUrl: process.env.NEXT_PUBLIC_MAILCHIMP_SIGNUP_FORM_URL,
};
