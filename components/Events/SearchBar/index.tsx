import {
  Editable,
  EditableInput,
  EditablePreview,
  Box,
  Select,
} from '@chakra-ui/react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import styles from './search.module.scss';

interface Props {
  onSubmit: (input: string) => void;
  onChangeIndustry: (e: any) => void;
  onChangeCountry: (e: any) => void;
  onChangeType: (e: any) => void;
  onChangeSource: (e: any) => void;
  countries: string[];
  industries: string[];
  types: string[];
  sources: string[];
  startDate: Date;
  onChangeStartDate: (date: Date) => void;
}

export default function EventSearchResult(props: Props) {
  const {
    onSubmit,
    onChangeIndustry,
    onChangeCountry,
    onChangeType,
    onChangeSource,
    countries,
    industries,
    types,
    sources,
    startDate,
    onChangeStartDate,
  } = props;
  // TODO: Date picker style
  // https://github.com/chakra-ui/chakra-ui/issues/580
  // https://gist.github.com/igoro00/99e9d244677ccafbf39667c24b5b35ed

  const today = new Date();

  const onChangeDate = (date: Date | [Date, Date] | null) => {
    if (!date) {
      return;
    }
    if (Array.isArray(date)) {
      // TODO: Handle date ranges
    } else {
      onChangeStartDate(date);
    }
  };

  return (
    <Box bg="white" my="15px">
      <Editable
        fontSize="xl"
        placeholder="Search for events"
        onSubmit={onSubmit}
      >
        <EditablePreview w="100%" color="gray.600" p="10px" />
        <EditableInput p="10px" />
      </Editable>

      <Box
        p="10px"
        display={{
          md: 'flex',
          base: 'block',
        }}
        className={styles.searchFilters}
      >
        <Select
          placeholder="All countries"
          flex="0 1 200px"
          onChange={onChangeCountry}
        >
          {countries.map((country) => (
            <option key={country} value={country}>
              {country}
            </option>
          ))}
        </Select>

        <Select
          placeholder="All industries"
          flex="0 1 200px"
          onChange={onChangeIndustry}
        >
          {industries.map((industry) => (
            <option key={industry} value={industry}>
              {industry}
            </option>
          ))}
        </Select>

        <Select
          placeholder="All types"
          flex="0 1 200px"
          onChange={onChangeType}
        >
          {types.map((type) => (
            <option key={type} value={type}>
              {type}
            </option>
          ))}
        </Select>

        <Select
          placeholder="All sources"
          flex="0 1 200px"
          onChange={onChangeSource}
        >
          {sources.map((source) => (
            <option key={source} value={source}>
              {source}
            </option>
          ))}
        </Select>

        {/* https://reactdatepicker.com/ */}
        <DatePicker
          selected={startDate}
          onChange={onChangeDate}
          minDate={today}
          dateFormat="MMMM dd, yyyy"
        />
      </Box>
    </Box>
  );
}
