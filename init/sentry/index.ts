import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

// https://sentry.io/venture-bites/venturebites/getting-started/javascript-react/
export const initSentry = () => {
  if (process.browser) {
    Sentry.init({
      dsn:
        'https://d2705b0f95804e599cd8b3276fbe81f4@o555558.ingest.sentry.io/5685491',
      integrations: [new Integrations.BrowserTracing()],

      // Set tracesSampleRate to 1.0 to capture 100%
      // of transactions for performance monitoring.
      // We recommend adjusting this value in production
      tracesSampleRate: 1.0,
    });
  }
};
