import { Button } from '@chakra-ui/react';
import { ChangeEvent, FormEvent, useState } from 'react';
import styles from './mailchimp.module.scss';
import querystring from 'querystring';

const baseUrl = process.env.NEXT_PUBLIC_MAILCHIMP_SIGNUP_FORM_URL;

function MailchimpSignupForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [result, setResult] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);

  function clearForm() {
    clearError();
    setFirstName('');
    setEmail('');
    setLastName('');
    setResult('');
    setIsSubmitting(false);
  }

  function clearError() {
    setEmailError('');
  }

  async function sendToMailchimp() {
    const valueObj = {
      EMAIL: email,
      FNAME: firstName,
      LNAME: lastName,
    };
    const url = `${baseUrl}&${querystring.stringify(valueObj)}`;

    setIsSubmitting(true);
    const result = await fetch(url, {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: '',
    });
    console.log(result);
    setIsSubmitting(false);
    setResult('Thank you for subscribing!');

    setTimeout(() => {
      clearForm();
    }, 5000);
  }

  async function onSubmit(e: FormEvent) {
    e.preventDefault();
    clearForm();
    if (!email) {
      setEmailError('Email is required');
      return;
    }
    await sendToMailchimp();
  }

  function onChangeEmail(e: ChangeEvent<HTMLInputElement>) {
    setEmail(e.target.value);
  }

  function onChangeFirstName(e: ChangeEvent<HTMLInputElement>) {
    setFirstName(e.target.value);
  }

  function onChangeLastName(e: ChangeEvent<HTMLInputElement>) {
    setLastName(e.target.value);
  }

  return (
    <form onSubmit={onSubmit} className={styles.form}>
      <div className={styles.header}>
        Subscribe to keep up with the latest tech events in Asia
      </div>
      <div>
        <label htmlFor="email">Email</label>
        {emailError && <p className={styles.error}>{emailError}</p>}
        <input
          id="email"
          type="email"
          name="EMAIL"
          placeholder="Email"
          value={email}
          onChange={onChangeEmail}
        />
      </div>

      <div>
        <label htmlFor="firstName">First name</label>
        <input
          id="firstName"
          type="text"
          name="FNAME"
          placeholder="First name"
          value={firstName}
          onChange={onChangeFirstName}
        />
      </div>

      <div>
        <label htmlFor="lastName">Last name</label>
        <input
          type="text"
          name="LNAME"
          placeholder="Last name"
          value={lastName}
          onChange={onChangeLastName}
        />
      </div>

      <div>
        <Button type="submit" isLoading={isSubmitting}>
          Subscribe
        </Button>
        {result && <p>{result}</p>}
      </div>
    </form>
  );
}

export default MailchimpSignupForm;
