import '../styles/globals.scss';
// import 'focus-visible/dist/focus-visible'
import { AppProps, NextWebVitalsMetric } from 'next/app';
import { ChakraProvider } from '@chakra-ui/react';
import { initGoogleTagManager } from '../init/googleTagManager';
import { initSentry } from '../init/sentry';
import { theme } from '../themes';

initGoogleTagManager();
initSentry();

export function reportWebVitals(metric: NextWebVitalsMetric) {
  console.log(metric);
}

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={theme}>
      <style jsx>{`
        :global(.js-focus-visible) :focus:not(.focus-visible) {
          outline: none;
        }
      `}</style>
      <Component {...pageProps} />
    </ChakraProvider>
  );
}

export default MyApp;
