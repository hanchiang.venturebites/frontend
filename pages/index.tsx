import Head from 'next/head';
import { useState, useEffect, BaseSyntheticEvent } from 'react';
import NavBar from '../components/Navbar';
import { Box, Heading, Spinner } from '@chakra-ui/react';
import { useEventsInfinite } from '../api';
import EventSearchResult from '../components/Events/EventSearchResult';
import {
  types,
  countries,
  industries,
  sources,
} from '../data/eventAggregateFields';
import LoadMoreButton from '../components/Events/LoadMorebutton';
import SearchBar from '../components/Events/SearchBar';
import { sendGAEvent } from '../init/googleTagManager';
import { GAEvents } from '../init/googleTagManager/types';
import { EventProp } from '../components/Events/types';
import MailchimpSignupForm from '../components/Mailchimp';
import Footer from '../components/Footer';
import { isLoadingMore, lastFetchedResult } from '../utils/events';
import styles from './home.module.scss';

const horizontalPadding = { base: '20px', sm: '30px', md: '50px', lg: '60px' };

function Home() {
  const [searchTerm, setSearchTerm] = useState('');
  const [country, setCountry] = useState('');
  const [industry, setIndustry] = useState('');
  const [type, setType] = useState('');
  const [source, setSource] = useState('');
  const [startDate, setStartDate] = useState(new Date());
  // Used for determining whether to trigger GA event. If page just mounted, don't trigger
  let justMounted = true;

  // TODO: Might need to refactor
  useEffect(() => {
    sendGAEvent(GAEvents.PageView, {
      pageView: {
        url: window.location.href,
      },
    });
  }, []);

  const buildQueryObject = () => {
    const result: any = { startDate: startDate.toISOString() };
    if (searchTerm) result.searchTerm = searchTerm;
    if (country) result.country = country;
    if (industry) result.industry = industry;
    if (type) result.type = type;
    if (source) result.source = source;
    return result;
  };

  const ongoingEventQuery = () => {
    const query = buildQueryObject();
    query.ongoing = 'true';
    return query;
  };

  const upcomingEventQuery = () => {
    const query = buildQueryObject();
    query.upcoming = 'true';
    return query;
  };

  const onChangeCountry = (e: BaseSyntheticEvent) => {
    setCountry(e.target.value);
  };

  const onChangeIndustry = (e: BaseSyntheticEvent) => {
    setIndustry(e.target.value);
  };

  const onChangeType = (e: BaseSyntheticEvent) => {
    setType(e.target.value);
  };

  const onChangeSource = (e: BaseSyntheticEvent) => {
    setSource(e.target.value);
  };

  const onSearchSubmit = (input: string) => {
    setSearchTerm(input);
  };

  const onClickEvent = (event: EventProp) => {
    const { url } = event;
    sendGAEvent(GAEvents.ExternalLinkClick, {
      externalUrl: url,
    });
  };

  // TODO: Refactor this shit
  // ongoing events
  const {
    size: ongoingEventPage,
    setSize: setOngoingEventPage,
    data: ongoingResult,
    error: ongoingError,
    isLoading: ongoingIsLoadingInitial,
  } = useEventsInfinite(ongoingEventQuery());

  const ongoingIsLoadingMore = isLoadingMore(
    ongoingIsLoadingInitial,
    ongoingEventPage,
    ongoingResult
  );
  const lastFetchedOngoingEventResult = lastFetchedResult(ongoingResult);
  const {
    count: ongoingEventsCount,
    data: lastFetchedOngoingEvents,
    hasNextPage: hasMoreOngoingEvents,
  } = lastFetchedOngoingEventResult;

  const ongoingEvents: any[] = [];
  ongoingResult?.forEach((result) => {
    if (!justMounted) {
      if (ongoingEvents.length === 0) {
        sendGAEvent(GAEvents.EventSearch, {
          eventSearch: {
            ...ongoingEventQuery(),
            page: 1,
          },
        });
      } else if (ongoingEvents.length > 0) {
        sendGAEvent(GAEvents.EventSearchLoadMore, {
          eventSearch: {
            ...ongoingEventQuery(),
            page: ongoingEventPage,
          },
        });
      }
    }
    justMounted = false;
    ongoingEvents.push(...result?.payload?.data);
  });

  // upcoming events
  const {
    size: upcomingEventPage,
    setSize: setUpcomingEventPage,
    data: upcomingResult,
    error: upcomingError,
    isLoading: upcomingIsLoadingInitial,
  } = useEventsInfinite(upcomingEventQuery());
  const upcomingIsLoadingMore = isLoadingMore(
    upcomingIsLoadingInitial,
    upcomingEventPage,
    upcomingResult
  );

  const lastFetchedUpcomingEventResult = lastFetchedResult(upcomingResult);
  const {
    count: upcomingEventsCount,
    data: lastFetchedupcomingEvents,
    hasNextPage: hasMoreupcomingEvents,
  } = lastFetchedUpcomingEventResult;

  const upcomingEvents: any[] = [];
  upcomingResult?.forEach((result) => {
    if (!justMounted) {
      if (upcomingEvents.length === 0) {
        sendGAEvent(GAEvents.EventSearch, {
          eventSearch: {
            ...upcomingEventQuery(),
            page: 1,
          },
        });
      } else if (upcomingEvents.length > 0) {
        sendGAEvent(GAEvents.EventSearchLoadMore, {
          eventSearch: {
            ...upcomingEventQuery(),
            page: upcomingEventPage,
          },
        });
      }
    }
    justMounted = false;
    upcomingEvents.push(...result?.payload?.data);
  });

  const shouldDisplayLoadingSpinner = ongoingIsLoadingInitial || upcomingIsLoadingInitial;

  // TODO: Past events

  const ongoingHeaderText = !ongoingIsLoadingMore
    ? `${ongoingEventsCount} ongoing events`
    : null;
  const upcomingHeaderText = !upcomingIsLoadingMore
    ? `${upcomingEventsCount} upcoming events`
    : null;

  const onLoadMoreUpcomingEvents = () => {
    setUpcomingEventPage(upcomingEventPage + 1);
  };

  const onLoadMoreOngoingEvents = () => {
    setOngoingEventPage(ongoingEventPage + 1);
  };

  const title =
    'VentureBites | The most relevant news in Asia tech startup scene';
  const description = 'Search for exciting events in Asia tech startup scene';

  return (
    <>
      {/* TODO: refactor in the future
        https://www.netlify.com/blog/2020/05/08/improve-your-seo-and-social-sharing-cards-with-next.js/
      */}
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
        <meta name="description" content={description} />

        <meta property="og:title" content={title} key="ogtitle" />
        <meta property="og:description" content={description} key="ogdesc" />

        <title>{title}</title>
        <link rel="icon" href="/images/favicon.ico" />
      </Head>

      <NavBar horizontalPadding={horizontalPadding} />

      <Box bg="gray.100" px={horizontalPadding} py="20px">
        <Heading>Attend the Hottest Events in Asia</Heading>
        <SearchBar
          onSubmit={onSearchSubmit}
          onChangeIndustry={onChangeIndustry}
          onChangeCountry={onChangeCountry}
          onChangeType={onChangeType}
          onChangeSource={onChangeSource}
          countries={countries}
          industries={industries}
          sources={sources}
          types={types}
          startDate={startDate}
          onChangeStartDate={setStartDate}
        />

        {/* upcoming events */}
        {
          shouldDisplayLoadingSpinner ? (
            <div className={styles.spinner}>
              <Spinner size="xl" />  
            </div>
          ) : (
            <>
              <Box>
                <EventSearchResult
                  header={upcomingHeaderText}
                  events={upcomingEvents}
                  onClick={onClickEvent}
                  eventType="upcoming"
                />
                {hasMoreupcomingEvents && (
                  <LoadMoreButton
                    onClick={onLoadMoreUpcomingEvents}
                    disabled={upcomingIsLoadingMore}
                    isLoading={upcomingIsLoadingMore}
                  />
                )}
              </Box>
      
              <Box my="40px"></Box>
      
              {/* ongoing events */}
              <EventSearchResult
                header={ongoingHeaderText}
                events={ongoingEvents}
                onClick={onClickEvent}
                eventType="ongoing"
              />
              {hasMoreOngoingEvents && (
                <LoadMoreButton
                  onClick={onLoadMoreOngoingEvents}
                  disabled={ongoingIsLoadingMore}
                  isLoading={ongoingIsLoadingMore}
                />
              )}
            </>
          )
        }
       

        <MailchimpSignupForm />
      </Box>

      <Footer horizontalPadding={horizontalPadding} />
    </>
  );
}

const scrollToTop = () => {
  window.scrollTo({
    top: 0
  })
}

export default Home;
