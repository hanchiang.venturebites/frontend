import querystring from 'querystring';
import useSWR, { useSWRInfinite } from 'swr';
import { config } from '../config';

const BASE_URL = process.env.NEXT_PUBLIC_API_URL;

const headers = new Headers();
if (config.isLocalhost) {
  headers.set('app-name', 'venturebites');
}

const fetcher = (url: string) => fetch(url, { headers }).then((r) => r.json());

export const getEvents = (query: Record<string, any>) =>
  fetcher(`${BASE_URL}/events?${querystring.stringify(query)}`);

export const useEvents = (query: Record<string, any>) => {
  const url = `${BASE_URL}/events/?${querystring.stringify(query)}`;
  const { data, error } = useSWR(url, fetcher);
  return { data, error, isLoading: !error && !data };
};

export const useEventDetail = (eventId: string) => {
  const url = `${BASE_URL}/events/${eventId}`;
  const { data, error } = useSWR(url, fetcher);
  return { data, error, isLoading: !error && !data };
};

export const useEventsInfinite = (query: Record<string, any>) => {
  const { data, error, size, setSize } = useSWRInfinite(
    (page: number): string => {
      query.page = page + 1;
      const url = `${BASE_URL}/events/?${querystring.stringify(query)}`;
      return url;
    },
    fetcher
  );
  return { data, size, setSize, error, isLoading: !error && !data };
};
