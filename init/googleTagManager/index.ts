import TagManager from 'react-gtm-module';

const tagManagerArgs = {
  gtmId: 'GTM-P74LV2B',
};

export const initGoogleTagManager = () => {
  if (process.browser) {
    TagManager.initialize(tagManagerArgs);
  }
};

// TODO: Consider switching to https://github.com/elgorditosalsero/react-gtm-hook
// See if it solves the issue of dataLayer containing data from all previous events
export const sendGAEvent = (event: string, data: any) => {
  if (process.browser) {
    const args = {
      dataLayer: {
        ...data,
        event,
      },
    };
    TagManager.dataLayer(args);
  }
};
