import { chakra } from '@chakra-ui/react';
import Link from 'next/link';
import Image from 'next/image';
import { SocialIcon } from '../Footer';
import styles from './navbar.module.scss';

interface Props {
  horizontalPadding: any;
}

function NavBar(props: Props) {
  const { horizontalPadding } = props;

  return (
    <chakra.div
      className={styles.navbar}
      px={horizontalPadding}
      py={{ base: '10px' }}
    >
      <ul>
        <li>
          <Link href="/" passHref>
            <a>
              <Image
                src="/images/venturebites-circle-logo-500x500.png"
                alt="VentureBites logo"
                width={60}
                height={60}
              />
            </a>
          </Link>
        </li>
        <li>
          <Link href="/">Events</Link>
        </li>
      </ul>

      <SocialIcon />
    </chakra.div>
  );
}

export default NavBar;
