export interface EventProp {
  image: string;
  onClick: (event: EventProp) => void;
  startDate: string;
  onlineOfflineText: string;
  title: string;
  organizer: string;
  type: string;
  url: string;
  source: string;
}
