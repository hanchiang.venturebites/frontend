export const isLoadingMore = (
  isLoading: boolean,
  page: number,
  result: any[] | undefined
): boolean => {
  return (
    isLoading || (page > 0 && result?.[page - 1]?.payload?.data === undefined)
  );
};

export const lastFetchedResult = (result: any[] | undefined): any => {
  return result?.[result?.length - 1]?.payload || {};
};
