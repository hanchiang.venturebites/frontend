export enum GAEvents {
  EventSearch = 'event-search',
  EventSearchLoadMore = 'event-search-load-more',
  ExternalLinkClick = 'external-link-click',
  PageView = 'page-view',
}
