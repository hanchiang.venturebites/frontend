import { Button } from '@chakra-ui/react';
import styles from './home.module.scss';

interface Props {
  onClick: () => void;
  disabled: boolean;
  isLoading: boolean;
}

export default function LoadMoreButton(props: Props) {
  const { onClick, disabled, isLoading } = props;
  return (
    <Button
      onClick={onClick}
      disabled={disabled}
      m="20px 0 10px 0"
      py="30px"
      w="100%"
      bg="#174974"
      color="white"
      isLoading={isLoading}
      _hover={{ color: '#black' }}
      _active={{}}
    >
      Load more
    </Button>
  );
}
